#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <stdio.h>
#include <math.h>

#include "config.h"
#include "cpuinfo.h"
#include "cpuload.h"

const gchar *cputray_authors[] = { "Samuel BAUER (mazes_80)", NULL };
GtkWidget *cputray__icon_menu;
GtkWidget *cputray__icon_submenu;
GtkStatusIcon *cputray__status_icon;
GdkPixbuf *cputray__icon;
GdkPixbuf *cputray__logo;
GSList *cputray__governor_group;

cpuinfo info;

static void cputray__menu_item_destroy(GtkWidget* menu_item, gpointer data);
static void cputray__tray_menu_init( );

gboolean cputray__icon_refresh (gpointer data) {
	cairo_text_extents_t extents;
	cairo_surface_t *surface;
	cairo_format_t format;
	cairo_t *cr;
	gint width, height;
	float avg_freq;
	double use;
	gchar *tmp;
	gint x, y;

	if(cputray__icon)
	     g_object_unref( G_OBJECT( cputray__icon ) );
	cputray__icon = gdk_pixbuf_copy(cputray__logo);

	format = (gdk_pixbuf_get_has_alpha (cputray__icon)) ? CAIRO_FORMAT_ARGB32 : CAIRO_FORMAT_RGB24;
	width = gdk_pixbuf_get_width (cputray__icon);
	height = gdk_pixbuf_get_height (cputray__icon);
	surface = cairo_image_surface_create_for_data( gdk_pixbuf_get_pixels(cputray__icon),
	   								  format, width, height,
	   								  gdk_pixbuf_get_rowstride(cputray__icon));
	g_assert (surface != NULL);
	cr = cairo_create (surface);

	/* Draw the pixbuf */
	gdk_cairo_set_source_pixbuf (cr, cputray__icon, 0, 0);
	cairo_paint (cr);

	avg_freq = cpuinfo_get_average_frequency(info, 0);

	if(1==1) {
		cairo_pattern_t *pattern;
		pattern = cairo_pattern_create_linear(0.0, 0.0, width, 0.0);
		cairo_pattern_add_color_stop_rgba(pattern, 0, 0, 1, 1, 1);
		cairo_pattern_add_color_stop_rgba(pattern, 1, 0, 0, 1, 1);

		/* Draw a white rectangle border */
		cairo_set_source_rgb (cr, 1, 1, 1);
		cairo_rectangle (cr, width * .125, height * .675, width *.75, height *.125);
		cairo_set_line_width(cr, 1);
		cairo_stroke (cr);

		cairo_set_source(cr, pattern);
		cairo_rectangle (cr, width * .125 + 1, height * .675 + 1, width *.75 * avg_freq - 2, height *.125 - 2);
		cairo_fill (cr);
		cairo_pattern_destroy(pattern);
	}
	else if(1==1) { // TODO different rendering modes
		cairo_pattern_t *pattern;
		pattern = cairo_pattern_create_linear(0.0, 0.0, width, 0.0);
		cairo_pattern_add_color_stop_rgba(pattern, 1, 0, 0, 1, 1);
		cairo_pattern_add_color_stop_rgba(pattern, 0, 0, 1, 1, 1);

		/* Draw a white rectangle border */
		cairo_set_source_rgb (cr, 1, 1, 1);
		cairo_rectangle (cr, width * .125, height * .125, width *.125, height *.75);
		//cairo_rectangle (cr, width * .125, height * .675, width *.75, height *.125);
		cairo_set_line_width(cr, 1);
		cairo_stroke (cr);

		cairo_set_source(cr, pattern);
		cairo_rectangle (cr, width * .125 + 1, height * .875 - height * avg_freq * .75 - 1, width *.125 - 2, height * avg_freq * .75 - 2);
		cairo_fill (cr);
		cairo_pattern_destroy(pattern);
	}
	else {
		/* Draw a white rectangle border */
		cairo_set_source_rgb (cr, 1, 1, 1);
		cairo_rectangle (cr, width * .125, height * .675, width *.75, height *.125);
		cairo_set_line_width(cr, 1);
		cairo_stroke (cr);

		if(avg_freq < .25) cairo_set_source_rgb (cr, 0, 1, 0);
		else if(avg_freq < .5) cairo_set_source_rgb(cr, 1, 1, 0);
		else if(avg_freq < .75) cairo_set_source_rgb(cr, 1, .5, 0);
		else cairo_set_source_rgb(cr, 1, 0, 0);
		cairo_rectangle (cr, width * .125 + 1, height * .675 + 1, width *.75 * avg_freq - 2, height *.125 - 2);
		cairo_fill (cr);
	}

	/* draw cpus count */
	tmp = g_malloc(sizeof(gchar)*5);
	cpuinfo_get_cpu_online(info);
	g_sprintf(tmp, "%d", info->n_cpu);
	cairo_set_font_size(cr, 12);
	cairo_text_extents(cr, tmp, &extents);
	cairo_set_source_rgb(cr, 1, 1, 1);
	x=( extents.width + extents.x_bearing + 12 ) / 2;
	y=height - ( extents.height + 12 ) / 2;
	cairo_arc(cr, x, y, extents.height / 2 + 4, 0, 2 * M_PI );
	cairo_fill_preserve(cr);
	cairo_set_source_rgb (cr, 0.2, 0.3, 0.9);
	cairo_stroke(cr);
	x=extents.width + extents.x_bearing - 1;
	y=height - extents.height + 2;
	cairo_move_to(cr, x, y);
	cairo_text_path(cr, tmp);
	cairo_set_line_width(cr, 1.5);
	cairo_stroke(cr);
	g_free(tmp);

	/* draw cpus load */
	tmp = g_malloc(sizeof(gchar)*5);
	cpuload_update();
	cpuload_get_value(0, &use);
	g_sprintf(tmp, "%2d%%", (int)use );
	cairo_set_font_size(cr, 16);
	cairo_text_extents(cr, tmp, &extents);
	x = width / 2 - (extents.width / 2 + extents.x_bearing);
	y = height * 7 / 16 - (extents.height / 2 + extents.y_bearing);
	cairo_move_to(cr, x, y);
	cairo_text_path(cr, tmp);
	cairo_set_source_rgb (cr, 0.2, 1, 0.3);
	cairo_set_line_width(cr, 1.5);
	cairo_stroke_preserve(cr);
	cairo_set_source_rgb (cr, 0, 1, 1);
	cairo_fill(cr);
	g_free(tmp);

	/* Draw temperature */
	/* cpuinfo_get_temperature(info, 0);
	tmp = g_malloc(sizeof(char) * ( 4 + (info->temp == 0 ? 0 : (int)(log10(info->temp)))));
	g_sprintf(tmp, "%d°C", info->temp);
	cairo_set_source_rgb (cr, 0.2, 1, 0.3);
	cairo_set_font_size(cr, 14);
	cairo_text_extents(cr, tmp, &extents);
	x=width / 2 - ( extents.width - extents.x_bearing ) / 2;
	y=extents.height + 3;
	cairo_move_to(cr, x, y);
	cairo_text_path(cr, tmp);
	cairo_set_line_width(cr, 1.5);
	cairo_stroke_preserve(cr);
	cairo_set_source_rgb (cr, 0, 1, 1);
	cairo_fill(cr);
	g_free(tmp); */

	/* Free the allocated structures. */
	cairo_surface_destroy(surface);
	cairo_destroy(cr);

	gtk_status_icon_set_from_pixbuf( cputray__status_icon, cputray__icon );

	return(TRUE);
}

void cputray__tray_popup_menu ( GtkStatusIcon *status_icon,
								guint button,
								guint activate_time,
								gpointer user_data )
{
	GtkContainer *cont;
	cont = GTK_CONTAINER( cputray__icon_menu );
	gtk_container_foreach( cont, cputray__menu_item_destroy, NULL);

	cpuinfo_refresh(info);

	cputray__tray_menu_init( );
	gtk_menu_popup( GTK_MENU( cputray__icon_menu ),
					NULL,
					NULL,
					gtk_status_icon_position_menu,
					cputray__status_icon,
					button,
					activate_time );
}

void cputray__about_show ( GtkMenuItem *menuitem, gpointer user_data)
{
	 gtk_show_about_dialog (NULL,
							"program-name", "CPU Tray",
							"logo", cputray__logo,
							"title", "About cputray",
							"authors", cputray_authors,
							"version", "0.1",
							"comments", "How did you get this software\nMaybe you hacked my machine\nIt's still not diffused\nAnd I don't get Internet access\nYou are so tricky",
							"website", "none://none.fr",
#ifdef GTK3
							"license-type", GTK_LICENSE_GPL_2_0,
#endif
							NULL);
}

void cputray__boost_toggle ( GtkCheckMenuItem *checkmenuitem, gpointer user_data )
{
	cpuinfo_boost_toggle ( info );
}

void cputray__set_governor ( GtkRadioMenuItem *radiomenuitem, gpointer user_data )
{
	if(gtk_check_menu_item_get_active( GTK_CHECK_MENU_ITEM( radiomenuitem) ) )
		cpuinfo_set_governor ( info , (gchar *)user_data );
}

void cputray__set_frequency ( GtkRadioMenuItem *radiomenuitem, gpointer user_data )
{
	if(gtk_check_menu_item_get_active( GTK_CHECK_MENU_ITEM( radiomenuitem) ) )
		cpuinfo_set_frequency ( info , (gchar *)user_data );
}

static void cputray__tray_menu_init( )
{
	GtkWidget *cputray__icon_menu_item;

	int c_gov; /* governor iterator */
	int c_freq; /* governor iterator */

	cputray__governor_group = NULL;
	cputray__icon_submenu = gtk_menu_new( );

	/* Iterate through available frequencies */
	for(c_gov=0; info->gov[c_gov]; c_gov++) {
		/* userspace governor */
		if(g_strcmp0("userspace", info->gov[c_gov])==0) {
			cputray__icon_menu_item = gtk_menu_item_new_with_label( info->gov[c_gov] );
			gtk_menu_shell_append( GTK_MENU_SHELL( cputray__icon_menu ), cputray__icon_menu_item );
			gtk_widget_show( cputray__icon_menu_item );

			gtk_menu_item_set_submenu( GTK_MENU_ITEM( cputray__icon_menu_item ), cputray__icon_submenu );
			gtk_widget_show( cputray__icon_submenu );

			/* Iterate through available governors */
			for(c_freq=0; info->freq[c_freq]; c_freq++) {
				cputray__icon_menu_item = gtk_radio_menu_item_new_with_label( cputray__governor_group, info->freq[c_freq] );
				cputray__governor_group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (cputray__icon_menu_item));
				if(g_strcmp0(info->u_freq, info->freq[c_freq])==0 && g_strcmp0(info->u_gov, info->gov[c_gov])==0)
					gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (cputray__icon_menu_item), TRUE);
				gtk_menu_shell_append( GTK_MENU_SHELL( cputray__icon_submenu ), cputray__icon_menu_item );
				gtk_widget_show( cputray__icon_menu_item );
				g_signal_connect( G_OBJECT( cputray__icon_menu_item ), "toggled", G_CALLBACK( cputray__set_frequency ), info->freq[c_freq] );
			}
		  }
		  /* any governor */
		  else {
				cputray__icon_menu_item = gtk_radio_menu_item_new_with_label( cputray__governor_group, info->gov[c_gov] );
				cputray__governor_group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (cputray__icon_menu_item));
				if(g_strcmp0(info->u_gov, info->gov[c_gov])==0)
					gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (cputray__icon_menu_item), TRUE);
				gtk_menu_shell_append( GTK_MENU_SHELL( cputray__icon_menu ), cputray__icon_menu_item );
				gtk_widget_show( cputray__icon_menu_item );
				g_signal_connect( G_OBJECT( cputray__icon_menu_item ), "toggled", G_CALLBACK( cputray__set_governor ), info->gov[c_gov] );
		  }
	 }

	if(info->boost != -1) {
		cputray__icon_menu_item = gtk_separator_menu_item_new( );
		gtk_menu_shell_append( GTK_MENU_SHELL( cputray__icon_menu ), cputray__icon_menu_item );
		gtk_widget_show( cputray__icon_menu_item );

		cputray__icon_menu_item = gtk_check_menu_item_new_with_label( "Turbo Boost" );
		gtk_menu_shell_append( GTK_MENU_SHELL( cputray__icon_menu ), cputray__icon_menu_item );
		if(!info->boost_w)
			gtk_widget_set_sensitive( cputray__icon_menu_item, FALSE );
		if(info->boost)
			gtk_check_menu_item_set_active( GTK_CHECK_MENU_ITEM( cputray__icon_menu_item ), TRUE );
		gtk_widget_show( cputray__icon_menu_item );
		g_signal_connect( G_OBJECT( cputray__icon_menu_item ), "toggled", G_CALLBACK( cputray__boost_toggle ), NULL );
	}

	cputray__icon_menu_item = gtk_separator_menu_item_new( );
	gtk_menu_shell_append( GTK_MENU_SHELL( cputray__icon_menu ), cputray__icon_menu_item );
	gtk_widget_show( cputray__icon_menu_item );

#ifdef GTK3
	cputray__icon_menu_item = gtk_menu_item_new_with_mnemonic( "_About" );
#else
	cputray__icon_menu_item = gtk_image_menu_item_new_from_stock( GTK_STOCK_ABOUT, NULL );
#endif
	gtk_menu_shell_append( GTK_MENU_SHELL( cputray__icon_menu ), cputray__icon_menu_item );
	g_signal_connect( G_OBJECT( cputray__icon_menu_item ), "activate", G_CALLBACK( cputray__about_show ), NULL );
	gtk_widget_show( cputray__icon_menu_item );

#ifdef GTK3
	cputray__icon_menu_item = gtk_menu_item_new_with_mnemonic( "_Properties" );
#else
	cputray__icon_menu_item = gtk_image_menu_item_new_from_stock( GTK_STOCK_PROPERTIES, NULL );
#endif
	gtk_menu_shell_append( GTK_MENU_SHELL( cputray__icon_menu ), cputray__icon_menu_item );
	gtk_widget_hide( cputray__icon_menu_item );

#ifdef GTK3
	cputray__icon_menu_item = gtk_menu_item_new_with_mnemonic( "_Quit" );
#else
	cputray__icon_menu_item = gtk_image_menu_item_new_from_stock( GTK_STOCK_QUIT, NULL );
#endif
	gtk_menu_shell_append( GTK_MENU_SHELL( cputray__icon_menu ), cputray__icon_menu_item );
	g_signal_connect( G_OBJECT( cputray__icon_menu_item ), "activate", G_CALLBACK( gtk_main_quit ), NULL );
	gtk_widget_show( cputray__icon_menu_item );

	gtk_widget_show( cputray__icon_menu );
}

static void cputray__init( )
{
	gchar *file;

	info = cpuinfo_new( );
	cpuload_init(sysconf(_SC_NPROCESSORS_CONF));

	file = g_strconcat( PREFIX, "/share/cputray/cpu.png", NULL );
	cputray__logo = gdk_pixbuf_new_from_file( file, NULL );
	g_free( file );

	cputray__status_icon = gtk_status_icon_new( );
	cputray__icon_refresh(NULL);

	gtk_status_icon_set_visible( cputray__status_icon, TRUE );
	g_signal_connect( G_OBJECT( cputray__status_icon ), "popup-menu", G_CALLBACK( cputray__tray_popup_menu ), NULL );

	cputray__icon_menu = gtk_menu_new( );

	g_timeout_add(1000, cputray__icon_refresh, NULL);
}

static void cputray__menu_item_destroy(GtkWidget* menu_item, gpointer data)
{
	gtk_widget_destroy(menu_item);
}

static void cputray__end( )
{
	GtkContainer *cont;

	cpuinfo_destroy( &info );
	cpuload_destroy();

	cont = GTK_CONTAINER( cputray__icon_menu );
	gtk_container_foreach( GTK_CONTAINER( cputray__icon_menu ), cputray__menu_item_destroy, NULL);

	gtk_widget_destroy( cputray__icon_menu );
	g_object_unref( G_OBJECT( cputray__logo ) );
	g_object_unref( cputray__status_icon );
}

int main( int argc, char *argv[] )
{
	gtk_init( &argc, &argv );
	cputray__init( );
	gtk_main( );

	cputray__end( );
	return(0);
}
