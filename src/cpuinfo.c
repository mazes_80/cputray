#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "cpuinfo.h"

const gchar * s_sys =		"/sys/devices/system/cpu/";
const gchar * s_thermal =	"/sys/class/thermal/thermal_zone%d/temp";

gchar * s_boost =			"cpufreq/boost";
gchar * s_cur_freq =		"/cpufreq/scaling_cur_freq";

gchar * cpuinfo_get_current_frequency(cpuinfo info, gchar *cpu);
gchar * cpuinfo_get_current_governor(cpuinfo info, gchar *cpu);
gchar * cpuinfo_get_info(ssize_t size, ssize_t *retval, gchar *f_name);
int cpuinfo_get_domain(cpuinfo info);
int cpuinfo_get_frequencies(cpuinfo info);
int cpuinfo_get_governors(cpuinfo info);
int cpuinfo_set_info(const gchar *value, gchar *f_name);
void cpuinfo_get_cpu_boost(cpuinfo info);

void cpuinfo_get_cpu_boost(cpuinfo info) {
	size_t size, retval;
	gchar *f_name;
	char *value;
	FILE *boost;

	f_name = g_strconcat( s_sys, s_boost, NULL);
	boost = fopen(f_name, "r");

	if(!boost) {
		g_free(f_name);
		s_boost = "intel_pstate/no_turbo";
		f_name = g_strconcat( s_sys, s_boost, NULL);
		boost = fopen(f_name, "r");
	}
	if(!boost) {
		g_free(f_name);
		info->boost = -1;
		s_boost = "";
		return;
	}

	size = 3;
	value = (char *)malloc( sizeof(char) * size );
	retval = getline(&value, &size, boost);
	info->boost = atoi(value);
	if(s_boost[0]=='i')
		info->boost=!info->boost;
	free(value);

	/* Is boost param writeable */
	fclose (boost);
	boost = fopen(f_name, "a");
	g_free(f_name);
	if(!boost)
		info->boost_w = 0;
	else {
		info->boost_w = 1;
		fclose(boost);
	}
}

void cpuinfo_boost_toggle ( cpuinfo info ) {
	gchar *f_name;
	FILE *boost;

	if(!info->boost_w) return;

	f_name = g_strconcat( s_sys, s_boost, NULL);
	boost = fopen(f_name, "w");
	g_free(f_name);

	info->boost = !info->boost;
	if(s_boost[0]=='i')
		fprintf(boost, "%d", !info->boost);
	else
		fprintf(boost, "%d", info->boost);

	fclose(boost);
}

int cpuinfo_get_domain(cpuinfo info) {
	size_t size, retval;
	gchar *domain;
	int c, first, last;

	size = 1938; // Cpu limit = 512 (512*4-100-10=1938)

	domain = cpuinfo_get_info( size, &retval,
		g_strconcat(s_sys, "cpu0/topology/core_siblings_list", NULL) );
	if(!domain) {
		info->domain = NULL;
		return(-1);
	}

	for(c=0; domain[c]!='-'; c++)
		if(domain[c]=='\0') break;
	first = atoi(domain);
	if(domain[c]='-')
		last = atoi(&domain[c+1]);
	else
		last = first;
	g_free(domain);

	c = last - first + 2;
	info->domain = g_malloc(sizeof(gchar **) * c);
	info->domain[c-1] = NULL;
	last = last - first;
	for(c=0; c <= last; c++) {
		int length, val;
		val = c + first;
		length = (val == 0 ? 2 : (int)(log10(val)+2));
		info->domain[c] = g_malloc(sizeof(gchar *) * length);
		g_sprintf(info->domain[c], "%d", val);
	}

	 return(0);
}

void cpuinfo_get_cpu_online( cpuinfo info ) {
	info->n_cpu = sysconf (_SC_NPROCESSORS_ONLN);
	return;
}

int cpuinfo_get_frequencies(cpuinfo info) {
	size_t size, retval;
	gchar *frequencies;

	size = 256; // Arbitrary value

	frequencies = cpuinfo_get_info( size, &retval,
		g_strconcat(s_sys, "cpu", info->domain[0], "/cpufreq/scaling_available_frequencies", NULL) );
	if(!frequencies) {
		info->freq = NULL;
		return(-1);
	}
	if(frequencies) frequencies[retval-2]='\0';

	info->freq=g_strsplit(frequencies, " ", 0);
	g_free(frequencies);

	return(0);
}

gchar * cpuinfo_get_current_frequency(cpuinfo info, gchar *cpu) {
	size_t size, retval;
	gchar *u_freq;

	g_free(info->u_freq);
	info->u_freq = NULL;
	size = 11; // Until 999GHz, if needs to be changed, this software is legacy

	u_freq = cpuinfo_get_info( size, &retval,
		g_strconcat(s_sys, "cpu", cpu, s_cur_freq, NULL) );

	if(u_freq) u_freq[retval-1]='\0';

	return(u_freq);
}

int cpuinfo_get_governors(cpuinfo info) {
	size_t size, retval;
	gchar *governors;

	size = 57; // letters amount in all governors names, more 5 space + "\n\0"

	governors = cpuinfo_get_info( size, &retval,
		g_strconcat(s_sys, "cpu", info->domain[0], "/cpufreq/scaling_available_governors", NULL) );
	if(!governors) {
		info->gov = NULL;
		return(-1);
	}
	/* cpufreq-acpi appends ' ' to the string */
	if(governors[retval-2]==' ')
		governors[retval-2]='\0';
	else
		governors[retval-1]='\0';

	info->gov=g_strsplit(governors, " ", 0);
	g_free(governors);

	return(0);
}

gchar * cpuinfo_get_current_governor(cpuinfo info, gchar *cpu) {
	size_t size, retval;
	gchar *u_gov;

	g_free(info->u_gov);
	info->u_gov = NULL;
	size = 14; // conservative is 12 more 2

	u_gov = cpuinfo_get_info( size, &retval,
		g_strconcat(s_sys, "cpu", cpu, "/cpufreq/scaling_governor", NULL) );

	if(u_gov) u_gov[retval-1]='\0';

	return(u_gov);
}

void cpuinfo_set_frequency( cpuinfo info, const gchar *s_freq ) {
	ssize_t retval;
	gchar *f_name;
	int cpu, c;

	cpuinfo_set_governor(info, "userspace");

	f_name = g_malloc(56);
	for(cpu=0; info->domain[cpu]; cpu++) {
		g_sprintf( f_name, "%scpu%s%s", s_sys, info->domain[cpu], "/cpufreq/scaling_setspeed");
		retval = cpuinfo_set_info( s_freq , f_name );

		if(retval == 1) c++;
	}
	g_free(f_name);

	if(c == cpu) {
		g_free(info->u_freq);
		info->u_freq=g_strdup(s_freq);
	}
}

void cpuinfo_set_governor( cpuinfo info, const gchar *s_gov ) {
	ssize_t retval;
	gchar *f_name;
	int cpu, c;

	f_name = g_malloc(56);
	for(cpu=0,c=0; info->domain[cpu]; cpu++) {
		g_sprintf( f_name, "%scpu%s%s", s_sys, info->domain[cpu], "/cpufreq/scaling_governor");
		retval = cpuinfo_set_info( s_gov, f_name );

		if(retval == 1) c++;
	}
	g_free(f_name);

	if(c == cpu) {
		g_free(info->u_gov);
		info->u_gov=g_strdup(s_gov);
	}
}

float cpuinfo_get_average_frequency(cpuinfo info, int mode) {
	gchar *cur_freq, *min_freq, *max_freq;
	ssize_t retval;
	float avg_freq;
	int cpu;

	for(avg_freq=0.0,cpu=0; info->domain[cpu]; cpu++) {
		cur_freq = cpuinfo_get_info( 11, &retval,
			 g_strconcat(s_sys, "cpu", info->domain[cpu], s_cur_freq, NULL) );
		min_freq = cpuinfo_get_info( 11, &retval,
			 g_strconcat(s_sys, "cpu", info->domain[cpu], "/cpufreq/cpuinfo_min_freq", NULL) );
		max_freq = cpuinfo_get_info( 11, &retval,
			 g_strconcat(s_sys, "cpu", info->domain[cpu], "/cpufreq/cpuinfo_max_freq", NULL) );

		if (mode==0) avg_freq = avg_freq + atof(cur_freq) / atof(max_freq);
		else if (mode==1) avg_freq = avg_freq + ( atof(cur_freq) - atof(min_freq) ) / ( atof(max_freq) - atof(min_freq) );
		// else if(mode=2) // TODO percent following cur_freq pos in available_frequencies

		g_free(cur_freq); g_free(min_freq); g_free(max_freq);
	}
	avg_freq = avg_freq / cpu;
	return(avg_freq);
}

void cpuinfo_get_temperature(cpuinfo info, const int cpu) {
	size_t size, retval;
	gchar *temp, *f_name;

	size = (cpu == 0 ? 1 : (int)(log10(cpu)+1)) + 37;
	f_name = g_malloc(sizeof(gchar) * size);
	g_sprintf(f_name, s_thermal, cpu);

	temp = cpuinfo_get_info( size, &retval, f_name );
	info->temp = (atoi(temp)/1000);
	g_free(temp);
	return;
}

gchar * cpuinfo_get_info(ssize_t size, ssize_t *retval, gchar *f_name) {
	gchar *content;
	FILE *file;

	file = fopen(f_name, "r");
	g_free(f_name);

	if(!file) return(NULL);

	content = g_malloc(sizeof(gchar) * size);
	*retval = getline(&content, &size, file);
	fclose(file);
	return(content);
}

int cpuinfo_set_info(const gchar *value, gchar *f_name) {
	size_t size, retval;
	FILE *file;

	file = fopen(f_name, "w");

	if(!file) {
		g_printf("warning: cannot open for write: %s\n", f_name);
		return(-1);
	}

	size=strlen(value);
	retval = fwrite(value, size, 1, file);
	fclose(file);
	return(!retval);
}

cpuinfo cpuinfo_new() {
	struct stat buf;
	gchar *f_name;
	cpuinfo info;
	int i;

	info = (cpuinfo)malloc(sizeof(struct cpuinfo_));
	info->u_gov = NULL;
	info->u_freq = NULL;
	
	/* Some drivers doesn't provide scaling_cur_freq */
	/* Default is no read permission on cpuinfo_cur_freq see README */
	f_name = g_strconcat(s_sys, "cpu0", s_cur_freq, NULL);
	i = stat(f_name, &buf);
	if(i!=0) {
		g_free(f_name);
		s_cur_freq = "/cpufreq/cpuinfo_cur_freq";
		f_name = g_strconcat(s_sys, "cpu0", s_cur_freq, NULL);
		i = stat(f_name, &buf);
	}
	g_free(f_name);

	cpuinfo_get_domain(info);
	cpuinfo_get_cpu_boost(info);
	cpuinfo_get_governors(info);
	cpuinfo_get_frequencies(info);

	info->u_gov = cpuinfo_get_current_governor(info, info->domain[0]);
	info->u_freq = cpuinfo_get_current_frequency(info, info->domain[0]);

	return(info);
}

void cpuinfo_destroy(cpuinfo *info) {
	if(!info || !*info) return;

	g_strfreev((*info)->domain);
	g_strfreev((*info)->freq);
	g_strfreev((*info)->gov);
	g_free((*info)->u_freq);
	g_free((*info)->u_gov);

	free(*info);
}

void cpuinfo_refresh(cpuinfo info) {
	info->u_freq = cpuinfo_get_current_frequency(info, info->domain[0]);
	info->u_gov  = cpuinfo_get_current_governor (info, info->domain[0]);
}
