#include <glib.h>
#include <glib/gprintf.h>

#ifndef CPUTRAY__CPUINFO_H
#define CPUTRAY__CPUINFO_H
struct cpuinfo_ {
	gchar **gov;	// Available CPU frequency governors
	gchar *u_gov;	// Current frequency governor
	gchar **freq;	// Available CPU frequencies
	gchar *u_freq;	// Current frequency
	gchar **domain;	// Related CPU
	int n_cpu;		// Number of CPU present on this machine
	int n_gov;		// Amount of system CPU frequency governors
	
	int boost;		// Intel turbo boost flag
	int boost_w;	// able to change boost param
	
	int temp;		// CPU temperature
	
	// Per-CPU frequency
	// Per-CPU temperature
};

typedef struct cpuinfo_ * cpuinfo;

extern cpuinfo cpuinfo_new();
extern void cpuinfo_refresh(cpuinfo info);
extern void cpuinfo_destroy(cpuinfo *info);
extern void cpuinfo_boost_toggle ( cpuinfo info );
extern void cpuinfo_set_governor( cpuinfo info, const gchar *governor );
extern void cpuinfo_set_frequency( cpuinfo info, const gchar *frequency );
extern float cpuinfo_get_average_frequency(cpuinfo info, int mode);
extern void cpuinfo_get_cpu_online( cpuinfo info );
extern void cpuinfo_get_temperature(cpuinfo info, const int cpu);
#endif //CPUTRAY__CPUINFO_H
