#ifndef CPUTRAY__CPULOAD_H
#define CPUTRAY__CPULOAD_H
#define CPULOAD__BUF 1024

struct _cpuload {
	FILE *fp;
	unsigned int cpu;
	unsigned long long int *tick;	/* Total cpu tick */
	unsigned long long int *idle;	/* Total ticks idled */
	unsigned long long int *p_tick;	/* Previous tick value */
	unsigned long long int *p_idle;	/* Previous idle value */
	unsigned long long int *d_tick;	/* Total tick delta */
	unsigned long long int *d_idle;	/* Total idle delta */
};

typedef struct _cpuload * cpuload;

extern int cpuload_init(int n);
extern void cpuload_update();
extern void cpuload_get_value(int cpun, double *use);
extern int cpuload_destroy();
#endif //CPUTRAY__CPULOAD_H
