/* based on: http://phoxis.org/2013/09/05/finding-overall-and-per-core-cpu-utilization */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "cpuload.h"

cpuload load1;

int cpuload_read(FILE *fp, unsigned long long int *f);

int cpuload_init(int cpu) {
	unsigned long long int fields[10];
	FILE *fp;
	int i, c;

	fp = fopen("/proc/stat", "r");

	if(!fp) return(-1);

	load1 = malloc(sizeof(struct _cpuload));

	load1->fp = fp;
	load1->cpu = (unsigned int)cpu;
	load1->tick = malloc(sizeof(unsigned long long int) * cpu + 1 );
	load1->idle = malloc(sizeof(unsigned long long int) * cpu + 1 );
	load1->p_tick = malloc(sizeof(unsigned long long int) * cpu + 1 );
	load1->p_idle = malloc(sizeof(unsigned long long int) * cpu + 1 );
	load1->d_tick = malloc(sizeof(unsigned long long int) * cpu + 1 );
	load1->d_idle = malloc(sizeof(unsigned long long int) * cpu + 1 );

	c = 0;
	while (cpuload_read (fp, fields) != -1) {
		for (i=0, load1->tick[c] = 0; i<10; i++) {
			load1->tick[c] += fields[i];
		}
		load1->idle[c] = fields[3]; /* idle ticks index */
		c++;
	}

	return(0);
}

int cpuload_destroy() {
	free(load1->tick);
	free(load1->idle);
	free(load1->p_tick);
	free(load1->p_idle);
	free(load1->d_tick);
	free(load1->d_idle);

	fclose(load1->fp);
	free(load1);
}

int cpuload_read(FILE *fp, unsigned long long int *f)
{
	char buffer[CPULOAD__BUF];
	int retval;

	if (!fgets (buffer, CPULOAD__BUF, fp));
	/* Reading cpu entries */
	retval = sscanf (buffer, "c%*s %Lu %Lu %Lu %Lu %Lu %Lu %Lu %Lu %Lu %Lu",
		&f[0], &f[1], &f[2], &f[3], &f[4], &f[5], &f[6], &f[7], &f[8], &f[9]);
	if (retval == 0) return (-1);
	if (retval < 4) return(0); /* At least 4 fields are required */
	return(1);
}

void cpuload_update() {
	unsigned long long int fields[10];
	int i, cpu;

	fseek (load1->fp, 0, SEEK_SET);
	fflush (load1->fp);

	for (cpu = 0; cpu <= load1->cpu; cpu++) {
		/* Save n-1 value */
		load1->p_tick[cpu] = load1->tick[cpu];
		load1->p_idle[cpu] = load1->idle[cpu];

		/* Read n value */
		if (!cpuload_read(load1->fp, fields)) return;

		/* Update total and idle tick */
		for (i=0, load1->tick[cpu] = 0; i<10; i++) {
			load1->tick[cpu] += fields[i];
		}
		load1->idle[cpu] = fields[3];

		/* Update deltas */
		load1->d_tick[cpu] = load1->tick[cpu] - load1->p_tick[cpu];
		load1->d_idle[cpu] = load1->idle[cpu] - load1->p_idle[cpu];
	}
}

void cpuload_get_value(int cpun, double *use) {
	*use = ((load1->d_tick[cpun] - load1->d_idle[cpun]) / (double) load1->d_tick[cpun]) * 100;
	return;
}

/*int main (void) {
	int cpu, it;
	double percent_usage;

	cpu_load_init(2);

	it = 0;
	while (it < 10) {
		sleep (1);
		cpuload_update();
		for(cpu=0; cpu <= load1->cpu; cpu++) {
			cpuload_get_value(cpu, &percent_usage);
			if(cpu == 0)
				printf ("Total CPU Usage: %3.2lf%%\n", percent_usage);
			else
				printf ("\tCPU%d Usage: %3.2lf%%\n", cpu - 1, percent_usage);
		}
		printf ("\n");
		it++;
	}

	cpuload_destroy();
	return 0;
}*/
